package com.learnershutch.basics;

public class TestString {

    static String myString = "Ram";

    public static void main(String[] args) {
        System.out.println(myString);
        System.out.println("This is my name " + myString);
        String newString = myString + " \u00A9 2017";// the unicode will be printed
        System.out.println(newString);

        String s1 = "23.24";
        String s2 = "234.43";
        System.out.println(s1 + s2);// this will concatenate the strings and will not add.
        int i = 10;
        String sCon = s1 + i;
        System.out.println(sCon);// Again appended at the back of the string

        double d = 768.1314;
        System.out.println(d + s1);
    }
}
