package com.learnershutch.basics;

public class TestOperators {

    public static void main(String[] args) {
        int sum = 1 + 2;
        System.out.println("1 + 2 = "+sum);
        int newResult = sum - 1;
        System.out.println("3 - 1 = "+newResult);

        int mul = 2 * 8;
        System.out.println("2 * 8 ="+mul);

        int div = 4 / 2;
        System.out.println("4 / 2  = "+div);

        int remainder = 8763 % 4;
        System.out.println("8768 % 4 = "+remainder);

        sum++;
        System.out.println(sum);

        mul--;
        System.out.println(mul);

        div += 10;// div = div +10
        System.out.println(div);
        // As above we can do *= , /= , -=

        boolean isMy = false;
        if(isMy == false)// comparator and if statement
            System.out.println("I am False");
        // As above we can have same operators >= , <= etc


        int myScore = 99;
        if (myScore > 90 && myScore < 100)
            System.out.println("Nervous ninety");

    }
}
