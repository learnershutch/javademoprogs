package com.learnershutch.basics;

public class TestOperatorSecond {

    static int firstScore = 90;
    static int secondScore = 20;

    public static void main(String[] args) {
        if (firstScore > 80 || secondScore <30)
            System.out.println("Avg");

        boolean male = true;
        // Ternary operator
        boolean isMale = male ? true : false;// male is true or false and assign the result to isMale;
        System.out.println(isMale);
    }
}
