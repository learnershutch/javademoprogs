package com.learnershutch.basics;

public class TestClass {

    public static void main(String[] args) {
        int mynum = 12_23_876;
        long mylong = 2678635785732458L;

        byte myByte = 127;

       // byte newByte = (myByte/2); it will give compile time error as this will be converted to int.
        // so we need to cast the value.
        byte myNewByte = (byte)(myByte/2);
        System.out.println(myNewByte);

        short myShort = 213;
        short newShort = (short)(myShort/2);
        System.out.println(newShort);

        float myFloat = 2.5f;
        double myDouble = 3.4;// here a 'd' is not necessary.

        //if you don't want to write 'f' in the float declared number then.
        float myNewFLoat = (float)2.78; //basically this is like type casting of a double to float


        //char can contain things in the 2 byte range.
        char myChar = '$';
        char myFirstChar = '&';
        char unicode = 'Đ';
        // the other way you can print the unicode in the char is ...
        char newchar = '\u00E0';
        System.out.println("the unicode is "+newchar);


        boolean myBooleanFirst = true;
        boolean myBooleanSecond = false;














    }
}
