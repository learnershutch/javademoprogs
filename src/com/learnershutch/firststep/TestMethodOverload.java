package com.learnershutch.firststep;

public class TestMethodOverload {
    public static void main(String[] args) {
        int sum1 = addNumbers(1,2);
        System.out.println(sum1);
        double sum2 = addNumbers(1.2, 3.4);
        System.out.println(sum2);
        int sum3 = addNumbers(1,2,3);
        System.out.println(sum3);
    }

    public static int addNumbers(int a , int  b){
        return  a + b;
    }

    //Overloading with number of arguments
    public static int addNumbers(int a, int b, int c){
        return a + b + c;
    }

    //overloading with datatype of arguments
    public static double addNumbers(double a, double b){
        return a + b;
    }
}
