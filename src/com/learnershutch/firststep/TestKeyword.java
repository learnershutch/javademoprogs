package com.learnershutch.firststep;

public class TestKeyword {

    // refer https://docs.oracle.com/javase/tutorial/java/nutsandbolts/_keywords.html to get the keywords in java

    //int int=2   -- not possible, you can't use the keyword name as a variable name
    int int1 = 24;// absolutely correct

    public static void main(String[] args) {
        int myScore = 90;
        double kilometers = (100 * 1.785875);// '100 * 1.785875' is called as an expression

        if(myScore == 90) {
            System.out.println("you congrats");
        }

        myScore++;// This is a statement
        System.out.println(myScore);// this is also a statement
        System.out.println("is this" +
                " my name "+
                "really"); // this is still a correct statement
        int i = 10; float f = 1.89f;String name = "Ram"; // This is a valid case as well


    }


}
