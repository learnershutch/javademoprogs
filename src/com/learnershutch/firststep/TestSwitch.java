package com.learnershutch.firststep;

public class TestSwitch {

    public static void main(String[] args) {

        //Using if and else
        int i = 1;
        if (i == 1)
            System.out.println("The number is 1");
        else if (i == 2)
            System.out.println("It's 2");
        else
            System.out.println("neither one or tw0");


        //Switch statements
        // the switch parameter and the case values must be of same datatype
        int switchValue = 4;
        switch (switchValue) {
            case 1:
                System.out.println("the number is one");
                break;// if don't put break then it executes all statements - unless found a break
            case 2:
                System.out.println("the number is two");
                break;
            case 3: case 4: case 5: // we can also combine the case and give a statement
                System.out.println("it's either a 3 or 4 or 5");
                break;// if you don't use a break the it goes and executes the default statements code as well
            default: // if neither of the cases match then it goes to default
                System.out.println("the valie is neither one or two");
                break;// this will anyways not effect but good to use here as well
        }

        String switchMonth = "Jan";
        switch (switchMonth){
            case "Jan":
                System.out.println("The month is January");
                break;
            default:
                System.out.println("Any other month");
        }
    }
}
