package com.learnershutch.firststep;

public class TestForLoop {

    public static void main(String[] args) {
        double salary = 1000.50;

        // Bottom to top
        for (int i=2;i <=12; i++) {
            //System.out.println("Salary total in "+i + " Months are ... "+calculateTotalSalary(salary,i)); // This works fine but it shows one zero, In order to show two zeros
            // Do as below

            System.out.println("Total salary in "+ i+"  Months..."+String.format("%.2f",calculateTotalSalary(salary,i)));// %.2f will show two zeros after decimal
        }

        // Top to bottom
        for (int i=12;i >=2; i--) {
            //System.out.println("Salary total in "+i + " Months are ... "+calculateTotalSalary(salary,i)); // This works fine but it shows one zero, In order to show two zeros
            // Do as below

            System.out.println("Total salary in "+ i+"  Months..."+String.format("%.2f",calculateTotalSalary(salary,i)));// %.2f will show two zeros after decimal
        }


        int num = 12;
        int num1 = 13;
        System.out.println(isPrime(num));
        System.out.println(isPrime(num1));

        System.out.println(isPrimeOptimized(num));
        System.out.println(isPrimeOptimized(num1));
    }

    public static double calculateTotalSalary(double salary,int numnerOfMonths) {
        return salary * numnerOfMonths;
    }


    public static boolean isPrime(int number) {

        if (number == 1) {
            return false;
        }

        // count till the half way till the number
        for (int i = 2; i <= number/2; i++) {
            if(number % i == 0)
                return false;
        }

        return true;
    }

    public static boolean isPrimeOptimized(int number) {

        if (number == 1) {
            return false;
        }

        //count till the
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if(number % i == 0)
                return false;
        }

        return true;
    }
    
}