package com.learnershutch.firststep;

public class TestMethod {

    public static void main(String[] args) {
        System.out.println(myName());// calling the static method directly
        myDetails("Ram","Amazon",12,9.2);

        // If the exam completed then return the answer
        System.out.println("Total mark is ... " +totalMark(90,98,60,true));
    }

    private static String myName(){
        return "Ram";
    }

    private static void myDetails(String name, String company, int age, double cgpa) {
        System.out.println("My details are ::: "+name +" "+company+ " "+ age+ " "+cgpa);
    }

    private static int totalMark(int math, int science, int english, boolean examCompleted) {
        int total = math + science + english;
        if (examCompleted) {
            return total;
        }
        return -1;
    }
}
