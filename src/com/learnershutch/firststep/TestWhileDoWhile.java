package com.learnershutch.firststep;

public class TestWhileDoWhile {

    public static void main(String[] args) {
        int i = 1;//initialization
        while (i < 10) {//condition
            System.out.println("The i value is " + i);
            i++;// increment
        }

        while (true){ //infinite loop
            if(i == 10)
                break;
            i++;
            System.out.println("Value of i is ..."+ i);
        }

        do {

            System.out.println("i value is ..."+ i);
            i++;
        }while (i <= 15);


        // Print all even numbers between a given range
        printEvenInNumberRange(0,10);

    }

    private static boolean isEven(int num) {
        if(num % 2 ==0)
            return true;
        else
            return false;
    }

    private static void printEvenInNumberRange(int start, int finish) {
        int i = start;
        while(i <= finish) {

            if(isEven(i))
                System.out.print(i +" , ");
            i++;
        }
    }

}
